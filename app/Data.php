<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = [
        'box', 
        'jenis_sdb_id', 
        'nasabah_id', 
        'kuasa1_id', 
        'kuasa2_id', 
        'start_sewa', 
        'end_sewa', 
        'user_id'
    ];
    
    public function nasabah()
    {
        return $this->belongsTo('App\Nasabah', 'nasabah_id');
    }

    public function kuasa1()
    {
        return $this->belongsTo('App\Kuasa', 'kuasa1_id');
    }

    public function kuasa2()
    {
        return $this->belongsTo('App\Kuasa', 'kuasa2_id');
    }

    public function jenis_sdb()
    {
        return $this->belongsTo('App\JenisSdb', 'jenis_sdb_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function kunjungan()
    {
    	return $this->hasMany('App\Kunjungan', 'data_id');
    }
}
