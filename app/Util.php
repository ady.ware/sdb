<?php

namespace App;

use DB;
use Mail;
use Hash;
use Auth;
use DateTime;
use DateInterval;

use App\User;

class Util
{
	// 2019-02-31 10:15:02 => 31-02-2019
	public static function date($tanggal)
	{
		if ($tanggal == null || $tanggal == '') {
			return '--/--/----';
		} else {
			$tanggal = substr($tanggal, 0, 10);
			return explode('-', $tanggal)[2].'-'.explode('-', $tanggal)[1].'-'.explode('-', $tanggal)[0];
		}
	}
	
	// 2019-02-31 10:15:02 => 10:15:02
	public static function dateToTime($tanggal, $HHmm = false)
	{
		if ($tanggal == null || $tanggal == '') {
			return '';
		} else {
			if ($HHmm) return substr(explode(' ', $tanggal)[1], 0, 5);
			else return explode(' ', $tanggal)[1];
		}
	}
}