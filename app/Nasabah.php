<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nasabah extends Model
{
    protected $fillable = [
        'nama', 
        'ktp', 
        'cif', 
        'rekening', 
        'alamat', 
        'telepon', 
        'ttd', 
        'foto'
    ];
    
    public function data()
    {
    	return $this->hasMany('App\Data', 'nasabah_id');
    }
}
