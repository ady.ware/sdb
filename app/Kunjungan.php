<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kunjungan extends Model
{
    protected $fillable = [
        'data_id', 
        'pengunjung',
        'start_kunjungan',
        'end_kunjungan',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function data()
    {
        return $this->belongsTo('App\Data', 'data_id');
    }
}
