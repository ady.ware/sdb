<?php
namespace App\Http\Controllers;

use App\Http\Requests;

use DB;
use Auth;

use Illuminate\Http\Request;

class BerandaController extends Controller
{
    public function get()
    {
        return view('beranda');
    }

    public function home()
    {
        // return 'aw';
        if (Auth::check() && !session('sukses') && !session('gagal')) {
            return redirect('/beranda');
        } else {
            return view('home');
        }
    }

    public function beranda_new()
    {
        return view('beranda_new');
    }
}
