<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Image;
use Datetime;

use App\Data;
use App\Kuasa;
use App\Nasabah;
use App\JenisSdb;
use App\Kunjungan;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class DataController extends Controller
{
    const image_list = [
        'nasabah_ttd', 'nasabah_foto', 'kuasa1_ttd', 'kuasa1_foto', 'kuasa2_ttd', 'kuasa2_foto'
    ];

    public function index()
    {
        $datas = Data::orderBy('id', 'DESC')->get();

        return view('data.index', compact('datas'));
    }

    public function create()
    {
        $nasabahs       = Nasabah::all();
        $nasabahJSON    = response()->json($nasabahs);
        $jenis_sdbs     = JenisSdb::all();

        return view('data.create', compact('jenis_sdbs', 'nasabahJSON', 'nasabahs'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        $images = [];
        
        if($request->nasabah_ttd) {
            $file_name  = time().'.'.$request->nasabah_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->nasabah_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->nasabah_foto) {
            $file_name  = time().'.'.$request->nasabah_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->nasabah_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa1_ttd) {
            $file_name  = time().'.'.$request->kuasa1_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa1_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa1_foto) {
            $file_name  = time().'.'.$request->kuasa1_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa1_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa2_ttd) {
            $file_name  = time().'.'.$request->kuasa2_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa2_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa2_foto) {
            $file_name  = time().'.'.$request->kuasa2_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa2_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }
        
        DB::beginTransaction();

        try {
            $start_sewa = new Datetime(explode(' - ', $request->pilihRentangTanggal)[0]);
            $end_sewa   = new Datetime(explode(' - ', $request->pilihRentangTanggal)[1]);

            if ($request->data_nasabah != '' || $request->data_nasabah != null) {
                $dataNasabah   = Nasabah::find($request->data_nasabah);

                $ttd    = $images[0] != '' ? $images[0] : $dataNasabah->ttd;
                $foto   = $images[1] != '' ? $images[1] : $dataNasabah->foto;

                $data = Nasabah::find($request->data_nasabah)->update(
                            ['nama'      => $request->nama,
                            'ktp'       => $request->ktp,
                            'cif'       => $request->cif,
                            'rekening'  => $request->rekening,
                            'alamat'    => $request->alamat,
                            'telepon'   => $request->telepon,
                            'ttd'       => $ttd,
                            'foto'      => $foto
                            ]);
            } else {
                $dataNasabah = Nasabah::create(
                                ['nama'      => $request->nama,
                                'ktp'       => $request->ktp,
                                'cif'       => $request->cif,
                                'rekening'  => $request->rekening,
                                'alamat'    => $request->alamat,
                                'telepon'   => $request->telepon,
                                'ttd'       => $images[0],
                                'foto'      => $images[1]
                                ]);
            }        

            $dataKuasa1 = null;
            if ($request->nama_kuasa1 != null) {
                $dataKuasa1 = Kuasa::create(
                            ['nama'         => $request->nama_kuasa1,
                                'ktp'       => $request->ktp_kuasa1,
                                'telepon'   => $request->telepon_kuasa1,
                                'catatan'   => $request->catatan_kuasa1,
                                'ttd'       => $images[2],
                                'foto'      => $images[3]
                            ]);
            }

            $dataKuasa2 = null;
            if ($request->nama_kuasa2 != null) {
                var_dump('kene lho1');die;
                $dataKuasa2 = Kuasa::create(
                                ['nama'      => $request->nama_kuasa2,
                                'ktp'       => $request->ktp_kuasa2,
                                'telepon'   => $request->telepon_kuasa2,
                                'catatan'   => $request->catatan_kuasa2,
                                'ttd'       => $images[4],
                                'foto'      => $images[5]
                                ]);
            }

            $data        = Data::create(
                            ['box'          => $request->box, 
                            'jenis_sdb_id'  => $request->jenis_sdb, 
                            'nasabah_id'    => $dataNasabah->id, 
                            'kuasa1_id'     => $request->nama_kuasa1 != null ? $dataKuasa1->id : null, 
                            'kuasa2_id'     => $request->nama_kuasa2 != null ? $dataKuasa2->id : null,
                            'start_sewa'    => $start_sewa, 
                            'end_sewa'      => $end_sewa, 
                            'user_id'       => Auth::user()->id,
                            ]);

            DB::commit();
            
            // all good
            return redirect('/data')->with('sukses', 'tambah');
        } catch (\Exception $e) {
            DB::rollback();
            for($i = 0; $i < sizeof($images); $i++) {
                $path = public_path('images/' . $images[$i]);
                File::delete($path);
            }
            // something went wrong
            return redirect('/data/create')->with('gagal', 'tambah');
        }
    }
    
    public function show($id)
    {
        $data               = Data::find($id);
        $kunjungan          = Kunjungan::where('end_kunjungan', null)->first();
        $start_kunjungan    = $kunjungan ? $kunjungan->start_kunjungan : null;
        $start_kunjungan    = $kunjungan ? $kunjungan->start_kunjungan : null;
        $done_start         = $kunjungan && $kunjungan->start_kunjungan != null ? 'true' : 'false';
        $ada_kunjungan      = $kunjungan ? 'true' : 'false';

        return view('data.show', compact('data', 'kunjungan', 'start_kunjungan', 'done_start', 'ada_kunjungan'));
    }

    public function edit($id)
    {
        $data       = Data::find($id);
        $jenis_sdbs = JenisSdb::all();
        
        return view('data.edit', compact('data', 'jenis_sdbs'));
    }

    public function update(Request $request, $id)
    {
        $data       = Data::find($id);
        $images     = [];
        
        // array_push($images, $request->nasabah_ttd ? time().'.'.$request->nasabah_ttd->getClientOriginalExtension() : '');
        // array_push($images, $request->nasabah_foto ? time().'.'.$request->nasabah_foto->getClientOriginalExtension() : '');
        // array_push($images, $request->kuasa1_ttd ? time().'.'.$request->kuasa1_ttd->getClientOriginalExtension() : '');
        // array_push($images, $request->kuasa1_foto ? time().'.'.$request->kuasa1_foto->getClientOriginalExtension() : '');
        // array_push($images, $request->kuasa2_ttd ? time().'.'.$request->kuasa2_ttd->getClientOriginalExtension() : '');
        // array_push($images, $request->kuasa2_foto ? time().'.'.$request->kuasa2_foto->getClientOriginalExtension() : '');

        if($request->nasabah_ttd) {
            $file_name  = time().'.'.$request->nasabah_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->nasabah_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->nasabah_foto) {
            $file_name  = time().'.'.$request->nasabah_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->nasabah_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa1_ttd) {
            $file_name  = time().'.'.$request->kuasa1_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa1_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa1_foto) {
            $file_name  = time().'.'.$request->kuasa1_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa1_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa2_ttd) {
            $file_name  = time().'.'.$request->kuasa2_ttd->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa2_ttd)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        if($request->kuasa2_foto) {
            $file_name  = time().'.'.$request->kuasa2_foto->getClientOriginalExtension();
            $path       = public_path('images/' . $file_name);
            
            Image::make($request->kuasa2_foto)->save($path);            
            array_push($images, $file_name);
        } else {
            array_push($images, '');
        }

        DB::beginTransaction();
        
        try {
            $start_sewa = new Datetime(explode(' - ', $request->pilihRentangTanggal)[0]);
            $end_sewa   = new Datetime(explode(' - ', $request->pilihRentangTanggal)[1]);
            
            $dataNasabah = Nasabah::find($data->nasabah_id)->update(
                            ['nama'      => $request->nama,
                            'ktp'       => $request->ktp,
                            'cif'       => $request->cif,
                            'rekening'  => $request->rekening,
                            'alamat'    => $request->alamat,
                            'telepon'   => $request->telepon,
                            'ttd'       => $images[0] != '' ? $images[0] : $data->nasabah->ttd,
                            'foto'      => $images[1] != '' ? $images[1] : $data->nasabah->foto
                            ]);

            $data_kuasa1_id = null;
            $data_kuasa2_id = null;
            
            if ($data->kuasa1_id != null && $request->nama_kuasa1 != null) {
                $dataKuasa1 = Kuasa::find($data->kuasa1_id)->update(
                                ['nama'     => $request->nama_kuasa1,
                                'ktp'       => $request->ktp_kuasa1,
                                'telepon'   => $request->telepon_kuasa1,
                                'catatan'   => $request->catatan_kuasa1,
                                'ttd'       => $images[2] != '' ? $images[2] : $data->kuasa1->ttd,
                                'foto'      => $images[3] != '' ? $images[3] : $data->kuasa1->foto
                                ]);

                $data_kuasa1_id = $data->kuasa1_id;
            } elseif ($request->nama_kuasa1 != null) {
                $dataKuasa1 = Kuasa::create(
                    ['nama'         => $request->nama_kuasa1,
                        'ktp'       => $request->ktp_kuasa1,
                        'telepon'   => $request->telepon_kuasa1,
                        'catatan'   => $request->catatan_kuasa1,
                        'ttd'       => $images[2],
                        'foto'      => $images[3]
                    ]);

                $data_kuasa1_id = $dataKuasa1->id;
            }

            if ($data->kuasa2_id != null && $request->nama_kuasa2 != null) {
                $dataKuasa1 = Kuasa::find($data->kuasa2_id)->update(
                                ['nama'     => $request->nama_kuasa2,
                                'ktp'       => $request->ktp_kuasa2,
                                'telepon'   => $request->telepon_kuasa2,
                                'catatan'   => $request->catatan_kuasa2,
                                'ttd'       => $images[4] != '' ? $images[4] : $data->kuasa2->ttd,
                                'foto'      => $images[5] != '' ? $images[5] : $data->kuasa2->foto
                                ]);

                $data_kuasa2_id = $data->kuasa2_id;
            } elseif ($request->nama_kuasa2 != null) {
                $dataKuasa1 = Kuasa::create(
                    ['nama'         => $request->nama_kuasa2,
                        'ktp'       => $request->ktp_kuasa2,
                        'telepon'   => $request->telepon_kuasa2,
                        'catatan'   => $request->catatan_kuasa2,
                        'ttd'       => $images[4],
                        'foto'      => $images[5]
                    ]);

                $data_kuasa2_id = $dataKuasa2->id;
            }

            $data        = Data::find($id)->update(
                            ['box'          => $request->box, 
                            'jenis_sdb_id'  => $request->jenis_sdb, 
                            'kuasa1'        => $data_kuasa1_id,
                            'kuasa2'        => $data_kuasa2_id,
                            'start_sewa'    => $start_sewa, 
                            'end_sewa'      => $end_sewa, 
                            'user_id'       => Auth::user()->id,
                            ]);

            DB::commit();
            
            // all good
            return redirect('/data')->with('sukses', 'ubah');
        } catch (\Exception $e) {
            DB::rollback();
            for($i = 0; $i < sizeof($images); $i++) {
                File::delete(public_path('images/' . $images[$i]));
            }
            // something went wrong
            return redirect('/data/edit/'.$id)->with('gagal', 'ubah');
        }
    }
    
    public function mulaiKunjungan($id)
    {
        DB::beginTransaction();
        
        try {
            $kunjungan                  = Kunjungan::where('data_id', $id)->where('end_kunjungan', null)->first();
            $kunjungan->start_kunjungan = new Datetime();
            $kunjungan->update();

            DB::commit();
            
            // all good
            return response()->json([
                'status'    => 'success',
                'start'     => $kunjungan->start_kunjungan
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            
            // something went wrong
            return response()->json([
                'status'    => 'failed',
            ]);
        }
    }

    public function selesaiKunjungan($id)
    {
        DB::beginTransaction();
        
        try {
            $kunjungan = Kunjungan::where('data_id', $id)->where('end_kunjungan', null)->update([
                'end_kunjungan' => new Datetime()
            ]);

            DB::commit();
            
            // all good
            return response()->json([
                'status'    => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            
            // something went wrong
            return response()->json([
                'status'    => 'failed',
            ]);
        }
    }

    public function delete($id)
    {
        $data   = Data::find($id);
        if ($data) {
            Kunjungan::where('data_id', $id)->delete();
            $data->delete();

            return redirect()->back()->with('sukses', 'delete');
        } else {
            return redirect()->back()->with('gagal', '404');
        }
    }
}
