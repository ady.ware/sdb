<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Image;
use Datetime;

use App\Data;
use App\Util;
use App\Kuasa;
use App\Nasabah;
use App\JenisSdb;
use App\Kunjungan;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class KunjunganController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Kunjungan $kunjungan)
    {
        //
    }

    public function edit(Kunjungan $kunjungan)
    {
        //
    }

    public function update(Request $request, Kunjungan $kunjungan)
    {
        //
    }

    public function cetak($id)
    {
        $kunjungan  = Kunjungan::find($id);
        $data       = Data::find($kunjungan->data_id);

        $nama   = '';
        
        if($kunjungan->pengunjung == 0) $nama .= $data->nasabah->nama;
        if($kunjungan->pengunjung == 1)  $nama .= $data->kuasa1->nama;
        if($kunjungan->pengunjung == 2)  $nama .= $data->kuasa2->nama;

        $arr['tanggal']    = Util::date($kunjungan->created_at);
        $arr['jam']        = Util::dateToTime($kunjungan->created_at, true);
        $arr['user']       = $kunjungan->user->nama;
        $arr['box']        = $kunjungan->data->box;
        $arr['nama']       = $nama;
        $arr['pemohon']    = explode(',', $nama)[0];
        $arr['rekening']   = $data->nasabah->rekening;

        return view('cetak.kunjungan', compact('arr'));
    }

    public function preCetak(Request $request, $id)
    {
        $data       = Data::find($id);
        $kunjungan  = Kunjungan::where('data_id', $id)->where('end_kunjungan', null)->first();

        $pengunjung = 0;
        if($request->nasabah == 1) $pengunjung = 0;
        if($request->kuasa1 == 1)  $pengunjung = 1;
        if($request->kuasa2 == 1)  $pengunjung = 2;

        if ($kunjungan) {
            Kunjungan::find($kunjungan->id)->update(
                        ['pengunjung'    => $pengunjung,
                        'user_id'   => Auth::user()->id,
                        ]);
            
            return response()->json([
                'status'    => 'success',
                'id'        => $kunjungan->id,
            ]);
        } else {
            $data   = Kunjungan::create(
                        ['data_id'      => $id, 
                        'pengunjung'    => $pengunjung,
                        'user_id'       => Auth::user()->id,
                        ]);

            return response()->json([
                'status'    => 'success',
                'id'        => $data->id,
            ]);
        }

        return response()->json([
            'status'    => 'failed'
        ]);

    }
}
