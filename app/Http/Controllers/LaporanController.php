<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use PDF;
use Image;
use Datetime;

use App\Util;
use App\Data;
use App\Kuasa;
use App\Nasabah;
use App\JenisSdb;
use App\Kunjungan;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function laporanHarian() {
        $date   = date('Y-m-d');
        $datas  = Kunjungan::where('created_at', 'like', $date.'%')->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        return view('laporan.harian', compact('datas', 'date'));
    }

    public function laporanHarianPost(Request $request) {
        $date   = $request->tanggal;
        $datas  = Kunjungan::where('created_at', 'like', $date.'%')->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        return view('laporan.harian', compact('datas', 'date'));
    }

    public function CetakLaporanHarian($tanggal) {
        $date   = $tanggal;
        $datas  = Kunjungan::where('created_at', 'like', $date.'%')->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        return view('cetak.harian', compact('datas', 'date'));
    }

    public function CetakLaporanHarianPDF($tanggal) {
        $date   = $tanggal;
        $f_name = 'laoran harian '.Util::date($date).'.pdf';
        $datas  = Kunjungan::where('created_at', 'like', $date.'%')->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        $pdf = PDF::loadview('cetak.harian', ['date' => $date, 'datas' => $datas]);
        return $pdf->download($f_name);
    }

    public function laporanNasabah() {
        $id         = null;
        $datas      = null;
        $dataShow   = false;
        $nasabahs   = Nasabah::all();
        $box        = 0;

        return view('laporan.nasabah', compact('datas', 'id', 'box', 'nasabahs', 'dataShow'));
    }

    public function GetBoxNasabah($id) {
        $boxs = Data::where('nasabah_id', $id)->pluck('box');

        return response()->json([
            'boxs' => $boxs
        ]);
    }

    public function laporanNasabahPost(Request $request) {
        $id         = $request->nasabah_id;
        $box        = $request->box;
        $data_id    = Data::where('nasabah_id', $id)->where('box', $request->box)->pluck('id');
        $datas      = Kunjungan::whereIn('data_id', $data_id)->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();
        $nasabahs   = Nasabah::all();
        $dataShow   = true;

        $nasabah    = Nasabah::find($id);
        $arr        = [
                        'box'   => $box,
                        'nama'  => $nasabah->nama,
                        'rekening'  => $nasabah->rekening,
                        ];

        return view('laporan.nasabah', compact('datas', 'id', 'box', 'nasabahs', 'dataShow', 'arr'));
    }

    public function CetakLaporanNasabah(Request $request) {
        $id         = $request->nasabah_id;
        $box        = $request->box;
        $data_id    = Data::where('nasabah_id', $id)->where('box', $request->box)->pluck('id');
        $datas      = Kunjungan::whereIn('data_id', $data_id)->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        $nasabah    = Nasabah::find($id);
        $arr        = [
                        'box'   => $box,
                        'nama'  => $nasabah->nama,
                        'rekening'  => $nasabah->rekening,
                        ];

        if (sizeof($datas) == 0) return redirect('/laporan-nasabah')->with('gagal', 'kosong');

        return view('cetak.nasabah', compact('datas', 'arr'));
    }

    public function CetakLaporanNasabahPDF(Request $request) {
        $id         = $request->nasabah_id;
        $box        = $request->box;
        $data_id    = Data::where('nasabah_id', $id)->where('box', $request->box)->pluck('id');
        $datas      = Kunjungan::whereIn('data_id', $data_id)->where('start_kunjungan', '!=', null)->where('end_kunjungan', '!=', null)->orderBy('updated_at', 'desc')->get();

        $nasabah    = Nasabah::find($id);
        $arr        = [
                        'box'   => $box,
                        'nama'  => $nasabah->nama,
                        'rekening'  => $nasabah->rekening,
                        ];

        if (sizeof($datas) == 0) return redirect('/laporan-nasabah')->with('gagal', 'kosong');

        $f_name     = 'laoran nasabah.pdf';
        $pdf        = PDF::loadview('cetak.nasabah', ['datas' => $datas, 'arr' => $arr]);

        return $pdf->download($f_name);
    }
}
