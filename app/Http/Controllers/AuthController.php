<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use DateTime;
use DateInterval;

use App\User;
use App\Util;
use App\Config;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class AuthController extends Controller
{
    public function getLogin()
    {
        if (Auth::check() && !session('sukses') && !session('gagal')) {
            return redirect('/');
        } else {
            return view('auth/login');
        }
    }

    public function postLogin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $user = User::where(DB::raw('BINARY `username`'), $username)->first();
        if ($user) {
            if (Hash::check($password, $user->password)) {
                Auth::login($user, false);
                return redirect('/login')->with('sukses', 'Berhasil');
            } else {
                return redirect('/login')->with([
                    'gagal' => 'not matched',
                    'username' => $username,
                    'password' => $password
                ]);
            }
        } else {
            return redirect('/login')->with([
                'gagal' => 'not found',
                'username' => $username,
                'password' => $password
            ]);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function getRegister()
	{
        if (Auth::check() && !session('sukses') && !session('gagal')) {
            return redirect('/');
        } else {
            return view('auth/register');
        }
	}

    public function postRegister(Request $request)
    {
        if ($request->master_password != ENV('MASTER_KEY')) 
            return redirect('/register')->with([
                'gagal'             => 'master_key',
                'username'          => $request->username,
                'password'          => $request->password,
                'akses'             => $request->akses,
                'master_password'   => $request->master_password
            ]);

        $user           = new User();
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->akses    = $request->akses;

        if ($user->save()) {
            return redirect('/')->with('sukses', 'regis');
        } else {
            return redirect('/register')->with('gagal', 'regis');
        }
    }

    public function checkUser($username) {
        return User::where('username', $username)->first(); 
    }

    public function getUbahPassword()
    {
        return view('auth.change_password');
    }

    public function postUbahPassword(Request $request)
    {
        $user = Auth::user();
        
        if (Hash::check($request->password, $user->password)) {
            if ($request->new_password == $request->new_password_confirmation){
                $user->password = Hash::make($request->new_password);
                if ($user->save()) {
                    return redirect('/beranda')->with('sukses', 'password');
                } else {
                    return redirect('/ubah_password')->with('gagal', 'password');
                }
            } else {
                return redirect('/ubah_password')->with('unmatch', 'password');
            }
        } else {
            return redirect('/ubah_password')->with('salah', 'password');
        }
    }
}
