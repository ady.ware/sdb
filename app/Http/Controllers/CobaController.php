<?php

namespace App\Http\Controllers;

use Hash;
use File;
use DateTime;
Use Validator;

use App\User;
use App\Data;
use App\Util;
use App\Kuasa;
use App\Nasabah;
use App\JenisSdb;
use App\Kunjungan;

use Illuminate\Http\Request;

class CobaController extends Controller
{
    public function coba()
    {
        $kunjungan = Kunjungan::where('data_id', 2)->where('end_kunjungan', null)->first();
        $kunjungan->start_kunjungan = new Datetime();
        $kunjungan->update();
        return $kunjungan;
        // return explode(',', 'cek')[0];
        // $users = User::all();
        // foreach ($users as $key => $value) {
        //     $value->password = Hash::make('admin');
        //     $value->update();
        // }
    }
}
