<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kuasa extends Model
{
    protected $fillable = [
        'nama', 
        'ktp', 
        'telepon', 
        'catatan', 
        'ttd', 
        'foto'
    ];
    
    public function data()
    {
    	return $this->hasMany('App\Data', 'kuasa_id');
    }
}
