<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
                 // ->hourly();
        // $schedule->command('backup:clean')->everyMinute();
        // $schedule->command('backup:run')->everyMinute();
        // $schedule->command('backup:run')->twiceDaily('13:08', '13:10');;
        $schedule->command('backup:clean')->dailyAt('08:00');
        $schedule->command('backup:run')->dailyAt('08:00');
        $schedule->command('backup:clean')->dailyAt('10:00');
        $schedule->command('backup:run')->dailyAt('10:00');
        $schedule->command('backup:clean')->dailyAt('12:00');
        $schedule->command('backup:run')->dailyAt('12:00');
        $schedule->command('backup:clean')->dailyAt('14:00');
        $schedule->command('backup:run')->dailyAt('14:00');
        $schedule->command('backup:clean')->dailyAt('16:00');
        $schedule->command('backup:run')->dailyAt('16:00');
        $schedule->command('backup:clean')->dailyAt('18:00');
        $schedule->command('backup:run')->dailyAt('18:00');
        $schedule->command('backup:clean')->dailyAt('20:00');
        $schedule->command('backup:run')->dailyAt('20:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
