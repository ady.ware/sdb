<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSdb extends Model
{
    public function data()
    {
    	return $this->hasMany('App\Data', 'jenis_sdb_id');
    }
}
