<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@home');

Route::post('/login', 'AuthController@postLogin');
Route::get('/login', 'AuthController@getLogin');
Route::get('/logout', 'AuthController@getLogout');

Route::get('/register', 'AuthController@getRegister');
Route::post('/register', 'AuthController@postRegister');

Route::get('/check-user/{username}', 'AuthController@checkUser');


Route::group(['middleware' => 'auth'], function() {
    Route::get('/beranda', 'BerandaController@get');
    
    Route::get('/data', 'DataController@index');
    Route::get('/data/create', 'DataController@create');
    Route::get('/data/edit/{id}', 'DataController@edit');
    Route::post('/data', 'DataController@store');
    Route::put('/data/{id}', 'DataController@update');
    Route::get('/data/{id}', 'DataController@show');
    Route::delete('/data/hapus/{id}', 'DataController@delete');
    Route::get('/data/mulai-kunjungan/{id}', 'DataController@mulaiKunjungan');
    Route::get('/data/selesai-kunjungan/{id}', 'DataController@selesaiKunjungan');
    
    Route::post('/kunjungan/cetak/data/{id}', 'KunjunganController@preCetak');
    Route::get('/kunjungan/cetak/{id}', 'KunjunganController@cetak');

    Route::get('/laporan-harian', 'LaporanController@laporanHarian');
    Route::get('/cetak/laporan-harian/{tanggal}', 'LaporanController@CetakLaporanHarian');
    Route::get('/cetak/laporan-harian/pdf/{tanggal}', 'LaporanController@CetakLaporanHarianPDF');
    Route::post('/laporan-harian', 'LaporanController@laporanHarianPost');
    Route::get('/laporan-box/{id}', 'LaporanController@GetBoxNasabah');
    Route::get('/laporan-nasabah', 'LaporanController@laporanNasabah');
    Route::post('/laporan-nasabah', 'LaporanController@laporanNasabahPost');
    Route::post('/cetak/laporan-nasabah/', 'LaporanController@CetakLaporanNasabah');
    Route::post('/cetak/laporan-nasabah/pdf/', 'LaporanController@CetakLaporanNasabahPDF');

    Route::get('/ubah_password', 'AuthController@getUbahPassword');
    Route::post('/ubah_password', 'AuthController@postUbahPassword');
});

Route::get('/coba', 'CobaController@coba');