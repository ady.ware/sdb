@extends('layouts.print')

@section('app.style')
	<style>
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<!--
		 /* Font Definitions */
		 .kanan{
        	text-align: right;
        }

        .x_content{
        	/*font-size: 12px;*/
        	line-height: 130%;
        }

        .gemuk {
        	font-weight: bold;
        }
/*
        @page {
        size: 210cm 297cm;
        margin: 30mm 30mm 30mm 30mm;
	    }*/

	    @media print {
	    	.make-grid(md);
	    	table: width="80%", margin-left:50px;
	    }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:595.3pt 841.9pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}
		
		table > tbody > tr > td.atasan{
            font-weight: bold;
            text-decoration: underline;
        }

        table > tbody > tr > td.bawahan{
            border-bottom: 1px solid;
        }

        table > tbody > tr > td.jumlah{
            font-weight: bold;
            border-top: 1px solid;
        }
	</style>
@endsection

@section('app.body')
<div style="margin: 0 50px;">	
	<div class="row" style="margin: 0;">
		<div class="col-md-12">
			<center style="padding-top: 25px">
				<STRONG>FORM KUNJUNGAN NASABAH SDB </STRONG><br/>
				<STRONG>CABANG JAKARTA TAMAN ISMAIL MARZUKI </STRONG>
			</center>
			<br>
		</div>
	</div>

	<div class="row" style="margin: 0;">
		<div class="col-md-12">
			<table style="border-top: 1px solid black; border-bottom: 1px solid black;">
				<tr>
					<td colspan="3">Yang bertanda tangan di bawah ini :</td>
				</tr>
				<tr>
					<td width="30%">Nomor Box </td>
					<td width="1%"> : </td>
					<td> {{ $arr['box'] }} </td>
				</tr>
				<tr>
					<td width="30%">Nama / Kuasa </td>
					<td width="1%"> : </td>
					<td> {{ $arr['nama'] }} </td>
				</tr>
				<tr>
					<td width="30%">Nomor Rekening </td>
					<td width="1%"> : </td>
					<td> {{ $arr['rekening'] }} </td>
				</tr>
				<tr>
					<td colspan="3">dengan ini mohon bantuan untuk membuka safe deposit box pada :</td>
				</tr>
				<tr>
					<td width="30%">Tanggal </td>
					<td width="1%"> : </td>
					<td> {{ $arr['tanggal'] }} </td>
				</tr>
				<tr>
					<td width="30%">Jam </td>
					<td width="1%"> : </td>
					<td> {{ $arr['jam'] }} </td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row" style="margin: 0;">
		<div class="col-md-12">
			<table style="width: 100%; max-width: 100%">
				<tr>
					<td class="text-center" style="width: 30%; padding-top: 100px;">Pemohon</td>
					<td class="text-center" style="width: 30%; padding-top: 100px;">Petugas</td>
					<td class="text-center" style="width: 40%; vertical-align: top;">Keterangan</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection

@section('app.script')
    <script type="text/javascript">
    	$(document).ready(function() {
			
			window.print();
		});
    </script>
@endsection