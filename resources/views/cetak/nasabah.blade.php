@extends('layouts.print')

@section('app.style')
	<style>
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<!--
		 /* Font Definitions */
		 .kanan{
        	text-align: right;
        }

        .x_content{
        	/*font-size: 12px;*/
        	line-height: 130%;
        }

        .gemuk {
        	font-weight: bold;
        }

	    @media print {
	    	.make-grid(md);
	    	table: width="80%", margin-left:50px;
	    }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:595.3pt 841.9pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}

		.table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            border: 1px solid #000 !important;
        }

		table th {
			text-align: center;
		}

		.table-bordered th,
        .table-bordered td {
            border: 1px solid #0000;
        }
	</style>
@endsection

@section('app.body')
<div style="margin: 0 50px;">	
	<div class="row" style="margin: 0;">
		<div class="col-md-12">
			<center style="padding-top: 25px">
				<STRONG>LAPORAN KUNJUNGAH NASABAH </STRONG>
			</center>
			<br>
			<br>
		</div>
	</div>

	<div class="row" style="margin: 0;">
		<div class="col-md-12">
			<table class="table table-bordered" style="margin-bottom: 0;">
				<tr>
					<td style="width: 20%; border-right: 1px solid #0000 !important;">Nomor Box</td>
					<td style="width: 10px; border-right: 1px solid #0000 !important;">:</td>
					<td>{{ $arr['box'] }}</td>
				</tr>
				<tr>
					<td style="width: 20%; border-right: 1px solid #0000 !important;">Nama</td>
					<td style="width: 10px; border-right: 1px solid #0000 !important;">:</td>
					<td>{{ $arr['nama'] }}</td>
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #0000 !important; width: 20%; border-right: 1px solid #0000 !important;">Rekening</td>
					<td style="border-bottom: 1px solid #0000 !important; width: 10px; border-right: 1px solid #0000 !important;">:</td>
					<td style="border-bottom: 1px solid #0000 !important;">{{ $arr['rekening'] }}</td>
				</tr>
			</table>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="10px">No</th>
						<th>Tanggal</th>
						<th>Waktu Kunjungan</th>
						<th>Pengunjung</th>
						<th>Petugas</th>
					</tr>
				</thead>
				<tbody>
					@foreach($datas as $num => $data)
					<tr id="{{$data->id}}">
						<td>{{ $num+1 }}</td>
						<td>{{ Util::date($data->updated_at) }}</td>
						<td>{{ Util::dateToTime($data->start_kunjungan, true) }}  -  {{ Util::dateToTime($data->end_kunjungan, true) }}</td>
						@if ($data->nasabah == 0)
							<td>{{ $data->data->nasabah->nama }}</td>
						@elseif ($data->kuasa1 == 1)
							<td>{{ $data->data->kuasa1->nama }}</td>
						@elseif ($data->kuasa2 == 2)
							<td>{{ $data->data->kuasa2->nama }}</td>
						@else
							<td></td>
						@endif
						<td>{{ $data->user->nama }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('app.script')
    <script type="text/javascript">
    	$(document).ready(function() {
			
			window.print();
		});
    </script>
@endsection