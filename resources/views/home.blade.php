@extends('layouts.blank')

@section('style')
    <style type="text/css">
        body {
            background-color: #f7f7f7;
        }
        /* .submit {
            float: left;
        } */        
    </style>
@endsection

@section('content')
    <div class="login_wrapper" style="max-width: 500px;">
        <div class="animate form login_form">
            <section class="login_content">
                <form>
                    <h1>SDB - BRANCH</h1>
                    <div>
                        <img src="{{ asset('images/logo.png') }}" class="img profile_img" style="vertical-align: middle; width: 60%">
                    </div>
                    <div style="margin-top: 20px">
                        {{-- <button class="btn btn-primary" onclick="window.location.href = 'https://w3docs.com';">Log-in</button> --}}

                        <a href="{{ url('/login') }}" class="btn btn-sm btn-primary">
                            <span>Login</span>
                        </a>
                        <a href="{{ url('/register') }}" class="btn btn-sm btn-success">
                            <span>Register</span>
                        </a>
                        {{-- <button class="btn btn-success">Register</button> --}}
                    </div>
                    <div class="clearfix"></div>
                </form>
            </section>
        </div>
    </div>
@endsection

@section('script')
@if (session('sukses') == 'regis')
    <script type="text/javascript">
        swal({
            title: 'Sukses!',
            text: 'Anda berhasil registrasi!',
            timer: 3000,
            type: 'success'
        });
    </script>
@endif

    <script type="text/javascript">
        setTimeout(function() {
            $('#spinner').hide();
        }, 500);
    </script>

@endsection
