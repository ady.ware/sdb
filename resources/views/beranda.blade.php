@extends('layouts.admin')

@section('title')
    <title>SDB | Beranda</title>
@endsection

@section('style')
    <style media="screen">
        .thumbnail {
            padding: 20px;
        }
        .jarak {
            padding-bottom: 15px;
        }
        .jarak i {
            font-size: 0.75em;
        }
        .jarak_kiri {
            margin-left: 10px;
        }
        .avatar-view {
            margin-top: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="jumbotron col-md-12">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>SDB</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    
    @if (session('sukses') == 'password')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Password berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        
    </script>
@endsection