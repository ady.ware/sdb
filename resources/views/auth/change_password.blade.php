@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Password</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Ubah Ubah Password</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('ubah_password') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group">
						<label class="control-label">Ubah Password Lama</label>
						<input class="form-control" type="password" name="password">
					</div>
					<div class="form-group">
						<label class="control-label">Ubah Password Baru</label>
						<input class="form-control" type="password" name="new_password" id="password">
						<span id='pesan'></span>
					</div>
					<div class="form-group">
						<label class="control-label">Konfirmasi Ubah Password</label>
						<input class="form-control" type="password" name="new_password_confirmation" id="confirm_password">
						<span id='message'></span>
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-edit"></i> <span>Simpan</span>
						</button>
						<button class="btn btn-sm btn-default" id="btnReset" type="button">
							<i class="fa fa-refresh"></i> Reset
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')

	@if (session('unmatch') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Gagal!',
				text: 'Konfirmasi password tidak cocok!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('salah') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Gagal!',
				text: 'Password lama salah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#confirm_password').on('keyup', function () {
		  	if ($('#password').val() == $('#confirm_password').val()) {
		    	$('#message').html('Kata Sandi Baru Sama').css('color', 'green');
		  	} else 
		    	$('#message').html('Kata Sandi Baru Tidak Sama').css('color', 'red');
		});		
	</script>
@endsection
