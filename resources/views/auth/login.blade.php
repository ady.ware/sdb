@extends('layouts.blank')

@section('style')
    <style type="text/css">
        body {
            background-color: #f7f7f7;
        }
        .submit {
            float: left;
        }
    </style>
@endsection

@section('content')
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form id="formLogin" method="post" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" required="" autofocus="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" name="password" value="{{old('password')}}" required="" />
                    </div>
                    <button id="buttonHide" class="btn btn-default submit" style="display:none"></button>
                    <div class="clearfix"></div>
                </form>
                <div>
                    <button id="submit_button" class="btn btn-default submit">Log in</button>
                    <button id="buttonBack" class="btn btn-default submit">Cancel</button>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'Berhasil')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Anda berhasil login!',
                timer: 3000,
                type: 'success',
                closeOnConfirm: true,
                onClose: () => {
                    window.location.replace("{{url('beranda')}}");
                }
            });
        </script>
    @elseif (session('gagal') == 'not matched')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Nama Pengguna dan Kata Sandi tidak cocok!',
                type: 'error'
            });
            $('input[name="username"]').val("{{session('username')}}");
            $('input[name="password"]').val("{{session('password')}}");
        </script>
    @elseif (session('gagal') == 'not_found')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Nama Pengguna tidak ditemukan!',
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == 'not found')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Nama Pengguna \"{{session('username')}}\" tidak ditemukan!',
                type: 'error'
            });
            $('input[name="username"]').val("{{session('username')}}");
            $('input[name="password"]').val("{{session('password')}}");
        </script>
    @endif

    <script type="text/javascript">
        setTimeout(function() {
            $('#spinner').hide();
        }, 500);

        $(document).ready(function() {
                
        });

        $(document).on('click', '#submit_button', function(e) {
            e.preventDefault();
            
            $('#buttonHide').trigger('click');
        });
        
        $(document).on('click', '#buttonBack', function(e) {
            e.preventDefault();
            
            window.location.href = '{{ url('/') }}';
        });

        </script>

@endsection
