@extends('layouts.blank')

@section('style')
    <style type="text/css">
        body {
            background-color: #f7f7f7;
        }

        .form-groupc {
            text-align: left;
            padding-top: 5px;
        }

        .submit {
            float: left;
        }
    </style>
@endsection

@section('content')
    <div class="login_wrapper" style="max-width: 500px;">
        <div class="animate form login_form">
            <section class="login_content">
                <form id="formLogin" method="post" action="{{ url('/register') }}">
                    {!! csrf_field() !!}
                    <h1>Create User</h1>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4 form-groupc">
                                <label>Username</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" required="" autofocus="" />
                                <p id="username-error" style="color: red;margin-top: -16px; text-align: left; display: none">Username telah terdaftar, harap isi yang lain!</p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4 form-groupc">
                                <label>Password</label>
                            </div>
                            <div class="col-md-8">
                                <input type="password" class="form-control" placeholder="Password" name="password" value="{{old('password')}}" required="" autofocus="" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 form-groupc">
                                <label>Akses</label>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control select2_single" name="akses" required="" value="{{ old('akses') }}">
                                    <option value="">--Pilih Akses--</option>
                                    <option value="0">Operator</option>
                                    <option value="1">Supervisor</option>
                                </select>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-4 form-groupc">
                                <label>Master Password</label>
                            </div>
                            <div class="col-md-8">
                                <input type="password" class="form-control" placeholder="Master Password" name="master_password" value="{{old('master_password')}}" required="" autofocus="" />
                            </div>
                        </div>
                    </div>
                    <button id="buttonHide" class="btn btn-default submit" style="display:none"></button>
                    <div class="clearfix"></div>
                </form>
                <div>
                    <button id="submit_button" class="btn btn-default submit">Create</button>
                    <button id="buttonBack" class="btn btn-default submit">Cancel</button>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('script')
    @if (session('gagal') == 'master_key')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Master key salah!',
                type: 'error'
            });
            
            $('input[name="username"]').val("{{ session('username') }}");
            $('input[name="password"]').val("{{ session('password') }}");
            $('select[name="akses"]').val("{{ session('akses')}}").change();
            $('input[name="master_password"]').val("{{ session('master_password') }}");
        </script>
    @endif

    <script type="text/javascript">
        setTimeout(function() {
            $('#spinner').hide();
        }, 500);

        $(document).ready(function() {
                
        });

        $(document).on('click', '#submit_button', function(e) {
            e.preventDefault();

            var username    = $('input[name="username"]').val();
            var url         = "{{ url('check-user') }}"+'/'+ username;

            if (username != '') {
                $.get(url, function(data) {
                    if (data == '') {
                        $('#buttonHide').trigger('click');
                        $('#username-error').hide();
                    } else {
                        $('#username-error').show();
                    }
                });
            } else {
                $('#username-error').hide();
                $('#buttonHide').trigger('click');
            }
        });
        
        $(document).on('click', '#buttonBack', function(e) {
            e.preventDefault();
            
            window.location.href = '{{ url('/') }}';
        });

        </script>

@endsection
