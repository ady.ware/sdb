@extends('layouts.admin')

@section('title')
    <title>SDB | Data SDB</title>
@endsection

@section('style')
    <style media="screen">
    </style>
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2>Data SDB Nasabah <i style="text-transform: capitalize; font-style: normal">{{ $data->nasabah->nama }}</i></h2>
                    <h2 class="pull-right timerCount">
                        <label id="minutes">00</label>
                        :
                        <label id="seconds">00</label>
                    </h2>
                    <h2 class="pull-right timerCount" style="padding-right: 10px;">Waktu Kunjungan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tr>
                                    <th colspan="2">Nasabah</th>
                                    @if ($kunjungan && $kunjungan->pengunjung == 0)
                                        <th class="text-right"><input name="nasabahCheckbox" class="Checkbox" type="checkbox" checked></th>
                                    @else
                                        <th class="text-right"><input name="nasabahCheckbox" class="Checkbox" type="checkbox"></th>
                                    @endif
                                </tr>
                                <tr>
                                    <td width="30%">No Box </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->box }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">Nama Nasabah </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->nasabah->nama }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">No Rekening </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->nasabah->rekening }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">Alamat </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->nasabah->alamat }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">Telepon </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->nasabah->telepon }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">No KTP </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->nasabah->ktp }} </td>
                                </tr>
                                <tr>
                                    <td width="30%">Jenis SDB </td>
                                    <td width="1%"> : </td>
                                    <td> {{ $data->jenis_sdb->nama }} [{{ $data->jenis_sdb->keterangan }}] </td>
                                </tr>
                                <tr>
                                    <td width="30%">Lama Sewa </td>
                                    <td width="1%"> : </td>
                                    <td> {{ Util::date($data->start_sewa) }} sd {{ Util::date($data->end_sewa) }} </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            @if($data->kuasa1_id != null)
                                <table class="table table-striped">
                                    <tr>
                                        <th colspan="2">Kuasa 1</th>
                                        @if ($kunjungan && $kunjungan->pengunjung == 1)
                                            <th class="text-right"><input name="kuasa1Checkbox" class="Checkbox" type="checkbox" checked></th>
                                        @else
                                            <th class="text-right"><input name="kuasa1Checkbox" class="Checkbox" type="checkbox"></th>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td width="30%">Nama </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa1->nama }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">No KTP </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa1->ktp }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Telepon </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa1->telepon }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Catatan </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa1->catatan }} </td>
                                    </tr>
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th class="text-center">Foto</th>
                                                <th class="text-center">TTD</th>
                                            </tr>
                                            <tr>
                                                <td><img src="{{URL::to('/images/'.$data->kuasa1->foto)}}" style="max-height: 100%; max-width: 100%; width: auto;"></td>
                                                <td><img src="{{URL::to('/images/'.$data->kuasa1->ttd)}}" style="max-height: 100%; max-width: 100%; width: auto;"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            @if($data->kuasa2_id != null)
                                <table class="table table-striped">
                                    <tr>
                                        <th colspan="2">Kuasa 2</th>
                                        @if ($kunjungan && $kunjungan->pengunjung == 2)
                                            <th class="text-right"><input name="kuasa2Checkbox" class="Checkbox" type="checkbox" checked></th>
                                        @else
                                            <th class="text-right"><input name="kuasa2Checkbox" class="Checkbox" type="checkbox"></th>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td width="30%">Nama </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa2->nama }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">No KTP </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa2->ktp }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Telepon </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa2->telepon }} </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Catatan </td>
                                        <td width="1%"> : </td>
                                        <td> {{ $data->kuasa2->catatan }} </td>
                                    </tr>
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th class="text-center">Foto</th>
                                                <th class="text-center">TTD</th>
                                            </tr>
                                            <tr>
                                                <td><img src="{{URL::to('/images/'.$data->kuasa2->foto)}}" style="max-height: 100%; max-width: 100%; width: auto;"></td>
                                                <td><img src="{{URL::to('/images/'.$data->kuasa2->ttd)}}" style="max-height: 100%; max-width: 100%; width: auto;"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-preview-upload" style="border: none">
                        <div class="row">
                            <img src="{{URL::to('/images/'.$data->nasabah->foto)}}" class="on-img-preview" style="max-height: 200px">
                        </div>
                        <div class="row">
                            <label class="control-label">Foto Nasabah</label>
                        </div>
                    </div>
                    <div class="image-preview-upload" style="border: none">
                        <div class="row">
                            <img src="{{URL::to('/images/'.$data->nasabah->ttd)}}" class="on-img-preview" style="max-height: 200px">
                        </div>
                        <div class="row">
                            <label class="control-label">Tanda Tangan Nasabah</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="display: grid;">
                            <button class="btn btn-md btn-default" id="btnCetak" disabled>CETAK</button>
                            <button class="btn btn-md btn-default" id="mulaiKunjungan" disabled>MULAI KUNJUNGAN</button>
                            <button class="btn btn-md btn-default" id="selesaiKunjungan" disabled>SELESAI KUNJUNGAN</button>
                            <a href="{{ url('data/') }}" class="btn btn-md btn-default">KEMBALI</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div id="formCetakContainer" style="display: none;">
                <form id="cetak-form" method="post" action="{{ url('kunjungan/cetak/'.$data->id) }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="nasabahCheckboxName" value="0">
                    <input type="hidden" name="kuasa1CheckboxName" value="0">
                    <input type="hidden" name="kuasa2CheckboxName" value="0">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'delete')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == '404')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Data tidak ditemukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        var minutesLabel    = document.getElementById("minutes");
        var secondsLabel    = document.getElementById("seconds");
        var totalSeconds    = 0;
        var start_kunjungan = null;

        function setTime() {
            ++totalSeconds;
            secondsLabel.innerHTML = pad(totalSeconds % 60);
            minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
        }

        function pad(val) {
            var valString = val + "";
            if (valString.length < 2) {
                return "0" + valString;
            } else {
                return valString;
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.timerCount').hide();
        
        $(document).ready(function() {
            var start_kunjungan = '{{ $start_kunjungan }}';
            var ada_kunjungan   = '{{ $ada_kunjungan }}';
            var done_start      = '{{ $done_start }}';

            if (start_kunjungan != null && start_kunjungan != '') {
                start_kunjungan = new Date(start_kunjungan);
                waktu_sekarang = new Date();
                totalSeconds = parseInt((waktu_sekarang.getTime() - start_kunjungan.getTime()) / 1000);
                $('.timerCount').show();
                setInterval(setTime, 1000);
                $('#selesaiKunjungan').prop('disabled', false);
            }
            
            if (done_start == 'false' && ada_kunjungan == 'true') {
                $('#mulaiKunjungan').prop('disabled', false);
            }

            checkCetakDisabled();
        });

        function checkCetakDisabled() {
            var checkNasabah    = $('input[name="nasabahCheckbox"]').prop('checked') ? true : false;
            var checkKuasa1     = $('input[name="kuasa1Checkbox"]').prop('checked') ? true : false;
            var checkKuasa2     = $('input[name="kuasa2Checkbox"]').prop('checked') ? true : false;

            if (checkNasabah) {
                $('input[name="kuasa1Checkbox"]').prop('checked', false);
                $('input[name="kuasa2Checkbox"]').prop('checked', false);
                $('input[name="kuasa1Checkbox"]').prop('disabled', true);
                $('input[name="kuasa2Checkbox"]').prop('disabled', true);
            } else if (checkKuasa1) {
                $('input[name="nasabahCheckbox"]').prop('checked', false);
                $('input[name="kuasa2Checkbox"]').prop('checked', false);
                $('input[name="nasabahCheckbox"]').prop('disabled', true);
                $('input[name="kuasa2Checkbox"]').prop('disabled', true);
            } else if (checkKuasa2) {
                $('input[name="nasabahCheckbox"]').prop('checked', false);
                $('input[name="kuasa1Checkbox"]').prop('checked', false);
                $('input[name="nasabahCheckbox"]').prop('disabled', true);
                $('input[name="kuasa1Checkbox"]').prop('disabled', true);
            } else {
                $('.Checkbox').prop('checked', false);
                $('.Checkbox').prop('disabled', false);
            }

            if (checkNasabah || checkKuasa1 || checkKuasa2) {
                $('#btnCetak').prop('disabled', false);
            } else {
                $('#btnCetak').prop('disabled', true);
            }
        }

        $(document).on('change', '.Checkbox', function(e) {
            e.preventDefault();

            var selector = 'input[name="'+$(this).attr('name')+'Name"]';

            if ($(this).prop('checked')) $(selector).val(1);
            else $(selector).val(0);

            checkCetakDisabled();
        });

        $(document).on('click', '#btnCetak', function(e) {
            e.preventDefault();
            
            var url_str    = "{{ url('kunjungan/cetak/data') }}"+'/'+`{{ $data->id }}`;
            var nasabah    = $('input[name="nasabahCheckbox"]').prop('checked') ? 1 : 0;
            var kuasa1     = $('input[name="kuasa1Checkbox"]').prop('checked') ? 1 : 0;
            var kuasa2     = $('input[name="kuasa2Checkbox"]').prop('checked') ? 1 : 0;
            var data_id    = '{{ $data->id }}';

            $.ajax({
                type: 'POST',
                url: url_str,
                data: {
                    'nasabah': nasabah,
                    'kuasa1': kuasa1,
                    'kuasa2': kuasa2
                },
                success:function(data) {
                    if (data.status == 'success') {
                        var win = window.open('{{ url('/kunjungan/cetak') }}'+`/${data.id}`);
                        if (win) {
                            //Browser has allowed it to be opened
                            win.focus();
                            $('#mulaiKunjungan').prop('disabled', false);
                        } else {
                            //Browser has blocked it
                            alert('Please allow popups for this website');
                        }
                    } else {
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '#mulaiKunjungan', function(e) {
            e.preventDefault();

            var url = "{{ url('data/mulai-kunjungan') }}"+'/'+`{{ $data->id }}`;
            $.get(url, function(data) {
                if (data.status == 'success') {
                    totalSeconds = 0;
                    $('.timerCount').show();
                    window.scrollTo(0,0);
                    setInterval(setTime, 1000);

                    swal({
                        title: 'Sukses!',
                        text: 'Kunjungan berhasil dimulai!',
                        timer: 3000,
                        type: 'success'
                    });

                    $('#mulaiKunjungan').prop('disabled', true);
                    $('#selesaiKunjungan').prop('disabled', false);
                } else {
                    location.reload();
                }
            });
        });

        $(document).on('click', '#selesaiKunjungan', function(e) {
            e.preventDefault();

            var url = "{{ url('data/selesai-kunjungan') }}"+'/'+`{{ $data->id }}`;
            $.get(url, function(data) {
                if (data.status == 'success') {
                    $('.timerCount').hide();

                    $('.Checkbox').each(function(index, el) {
                        $(el).prop('checked', false);
                    });

                    checkCetakDisabled();
                    $('#selesaiKunjungan').prop('disabled', true);
                    swal({
                        title: 'Sukses!',
                        text: 'Kunjungan berhasil diselesaikan!',
                        timer: 3000,
                        type: 'success'
                    });
                } else {
                    location.reload();
                }
            });
        });
        
    </script>
@endsection