@extends('layouts.admin')

@section('title')
    <title>SDB | Data SDB</title>
@endsection

@section('style')
    <style media="screen">
        
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data SDB</h2>
                <a href="{{ url('data/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Data">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="dataTables_scroll" style="width=45vw; overflow-y=scroll">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0; width: 100%" id="tableData" style="border: none">
                        <thead>
                            <tr id="custom_search">
                                <td></td>
                                <td><input type="text" id="search_box" placeholder="Cari No Box"></td>
                                <td></td>
                                <td><input type="text" id="search_nama" placeholder="Cari Nama Nasabah"></td>
                                <td><input type="text" id="search_rekening" placeholder="Cari Rekening Nasabah"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th>No Box</th>
                                <th>Jenis SDB</th>
                                <th>Nama Nasabah</th>
                                <th>No Rekening Nasabah</th>
                                <th>No KTP</th>
                                <th>Lama Sewa</th>
                                <th>Alamat</th>
                                {{-- <th>Operator</th> --}}
                                <th>Tanggal</th>
                                <th style="width: 50px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datas as $num => $data)
                            <tr id="{{$data->id}}">
                                <td>{{ $num+1 }}</td>
                                <td>{{ $data->box }}</td>
                                <td>{{ $data->jenis_sdb->nama }}</td>
                                <td>{{ $data->nasabah->nama }}</td>
                                <td>{{ $data->nasabah->rekening }}</td>
                                <td>{{ $data->nasabah->ktp }}</td>
                                <td>{{ Util::date($data->start_sewa) }}  -  {{ Util::date($data->end_sewa) }}</td>
                                <td>{{ $data->nasabah->alamat }}</td>
                                {{-- <td>{{ $data->user->nama }}</td> --}}
                                <td>{{ $data->created_at->format('d-m-Y h:i:s') }}</td>
                                <td class="text-center" style="display: flex">
                                    <a href="{{ url('data/'.$data->id) }}" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Lihat Data">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ url('data/edit/'.$data->id) }}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Ubah Data">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <button id="btnHapus" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Data">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
        </form>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
			var table = $('#tableData').DataTable({
                            "bAutoWidth"        : false,
                            "bScrollCollapse"   : true,
                            "bProcessing"       : true,
                            dom                 : `<'row'<'col-sm-6'l><'col-sm-6'f>>
                                                    <'row'<'col-sm-12'tr>>
                                                    <'row'<'col-sm-5'i><'col-sm-7'p>>`,
                            "responsive"        : true,
                        });

            $('#search_box').on('keyup', function(){
                table
                .column(1)
                .search(this.value)
                .draw();
            });
            
            $('#search_nama').on('keyup', function(){
                table
                .column(3)
                .search(this.value)
                .draw();
            });

            $('#search_rekening').on('keyup', function(){
                table
                .column(4)
                .search(this.value)
                .draw();
            });
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: 'Data yang anda pilih akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Cancel'
            }).then(function(isConfirm) {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url('data/hapus') }}' + '/' + id);
                $('#formHapusContainer').find('input[name="_method"]').val('delete');
                $('#formHapusContainer').find('form').submit();
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        
    </script>
@endsection
