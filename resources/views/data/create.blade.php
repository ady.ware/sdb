@extends('layouts.admin')

@section('title')
    <title>SDB | Tambah Data</title>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Data</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('data') }}" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="control-label">Pilih Data Nasabah</label>
									<select class="form-control" id="data_option" name="data_option">
										<option value="">--Pilih Data--</option>
										<option value="new">Buat Data Nasabah Baru</option>
										<option value="old">Cari Data Nasabah Lama</option>
									</select>
								</div>
								<div id="DataNasabahContainer" class="form-group" style="display: none">
									<label class="control-label">Data Nasabah</label>
									<select class="form-control select2_single" id="data_nasabah" name="data_nasabah">
										<option value="">--Pilih Nasabah--</option>
										@foreach($nasabahs as $nasabah)
											<option value="{{ $nasabah->id }}">{{ $nasabah->nama }} | ktp:{{ $nasabah->ktp }} | rek:{{ $nasabah->rekening }} | cif:{{ $nasabah->cif }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group form-handle">
									<label class="control-label">Nomor KTP</label>
									<input class="form-control" type="text" name="ktp" required="">
								</div>
								<div class="form-group form-handle">
									<label class="control-label">Nomor CIF</label>
									<input class="form-control" type="text" name="cif" required="">
								</div>
								<div class="form-group form-handle">
									<label class="control-label">Nomor Rekening</label>
									<input class="form-control" type="text" name="rekening" required="">
								</div>
								<div class="form-group form-input">
									<label class="control-label">Nama Lengkap</label>
									<input class="form-control" type="text" name="nama" required="">
								</div>
								<div class="form-group form-input">
									<label class="control-label">Alamat</label>
									<textarea name="alamat" class="form-control" style="resize: vertical;"></textarea>
								</div>
								<div class="form-group form-input">
									<label class="control-label">Nomor Telepon</label>
									<input class="form-control" type="text" name="telepon" required="">
								</div>
								<div class="form-group">
									<label class="control-label">Nomor Box</label>
									<input class="form-control" type="text" name="box" required="">
								</div>
								<div class="form-group">
									<label class="control-label">Jenis SDB</label>
									<select class="form-control" id="jenis_sdb" name="jenis_sdb" required="">
										<option value="">--Pilih Jenis SDB--</option>
										@foreach($jenis_sdbs as $jenis_sdb)
											<option value="{{ $jenis_sdb->id }}">{{ $jenis_sdb->nama }} [{{ $jenis_sdb->keterangan }}]</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label class="control-label">Tanggal Sewa</label>
									<input name="pilihRentangTanggal" id="pilihRentangTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Rentang Tanggal" readonly="">
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="control-label">Tanda Tangan Nasabah <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="nasabah_ttd" id="nasabah_ttd" class="fileInput">
								</div>
								<div class="form-group">
									<label class="control-label">Foto Nasabah <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="nasabah_foto" id="nasabah_foto" class="fileInput">
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="border-top: 2px solid #cccccc; margin-top: 15px">
						<div class="col-md-12">
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Tanda Tangan Kuasa 1 <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="kuasa1_ttd" id="kuasa1_ttd" class="fileInput kuasa1_form">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Foto Kuasa 1 <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="kuasa1_foto" id="kuasa1_foto" class="fileInput kuasa1_form">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Tanda Tangan Kuasa 2 <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="kuasa2_ttd" id="kuasa2_ttd" class="fileInput kuasa2_form">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Foto Kuasa 2 <span class="label-error">*</span> </label>
									<div class="image-preview-upload">
										<img src="{{URL::to('/images/img_default.png')}}" class="on-img-preview">
									</div>
									<input type="file" name="kuasa2_foto" id="kuasa2_foto" class="fileInput kuasa2_form">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="control-label">Nama</label>
									<input class="form-control kuasa1_form" type="text" name="nama_kuasa1">
								</div>
								<div class="form-group">
									<label class="control-label">Nomor KTP</label>
									<input class="form-control kuasa1_form" type="text" name="ktp_kuasa1">
								</div>
								<div class="form-group">
									<label class="control-label">telepon</label>
									<input class="form-control kuasa1_form" type="text" name="telepon_kuasa1">
								</div>
								<div class="form-group">
									<label class="control-label">Catatan</label>
									<textarea rows="3" name="catatan_kuasa1" class="form-control kuasa1_form" style="resize: vertical;"></textarea>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="control-label">Nama</label>
									<input class="form-control kuasa2_form" type="text" name="nama_kuasa2">
								</div>
								<div class="form-group">
									<label class="control-label">Nomor KTP</label>
									<input class="form-control kuasa2_form" type="text" name="ktp_kuasa2">
								</div>
								<div class="form-group">
									<label class="control-label">telepon</label>
									<input class="form-control kuasa2_form" type="text" name="telepon_kuasa2">
								</div>
								<div class="form-group">
									<label class="control-label">Catatan</label>
									<textarea rows="3" name="catatan_kuasa2" class="form-control kuasa2_form" style="resize: vertical;"></textarea>
								</div>
								
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 0;">
							<p><span class="label-error">*</span> : Ukuran maksimal file upload 200KB</p>
							<button class="btn btn-success" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Tambah</span>
							</button>
							<button class="btn btn-default" id="btnReset" type="button">
								<i class="fa fa-refresh"></i> Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Gagal!',
                text: 'Data gagal di tambah, silahkan isi ulang form data!',
                timer: 3000,
                type: 'error'
            });
        </script>
	@endif

	<script type="text/javascript">
		var img_type 		= ['jpg', 'jpeg', 'png'];
		var data_nasabah 	= '{!! json_encode($nasabahs) !!}';
		data_nasabah		= JSON.parse(data_nasabah);

		$(document).ready(function() {
            // $('#data_option').val('old').change();
        });

		$(document).on('focus', '#pilihRentangTanggal', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').daterangepicker({
                autoApply: true,
                calender_style: "picker_2",
                showDropdowns: true,
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: false
            }, function(start, end) {
                var awal = (start.toISOString()).substring(0,10);
                var akhir = (end.toISOString()).substring(0,10);
            });
        });

		$(document).on('change', '.fileInput', function(e) {
            e.preventDefault();

			var val = $(this).val();
			var ext = getExtension(val).toLowerCase();

			var img_type_check 	= img_type.includes(ext);
			var img_size_check	= this.files[0].size / 1024;

			if (!img_type_check) {
				$(this).val('');
				$(this).parents('.form-group').find('img').first().attr('src', '{{ asset('images/img_default.png') }}');

				swal({
					title: 'Gagal!',
					text: 'File yang anda pilih bukan gambar!',
					timer: 3000,
					type: 'error',
				});				
			} else if (img_size_check > 200) {
				$(this).val('');
				$(this).parents('.form-group').find('img').first().attr('src', '{{ asset('images/img_default.png') }}');

				swal({
					title: 'Gagal!',
					text: 'File yang anda pilih lebih dari 200KB!',
					timer: 3000,
					type: 'error',
				});

			} else {
				$(this).parents('.form-group').removeClass('image-error');
				readURL(this, $(this).attr('id'));
			}
		});

		function readURL(input, id) {
			var selector = $('#'+id);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
				
                reader.onload = function (e) {
                    selector.parents('.form-group').find('img').first().attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

		function getExtension(filename) {
			var parts = filename.split('.');
			return parts[parts.length - 1];
		}

		$(document).on('change', '#data_option', function(e) {
            e.preventDefault();
			
			var val = $(this).val();

			if (val == 'old') {
				$('#DataNasabahContainer').show();
				$('.form-handle').find('input').val('');
				$('.form-handle').find('select').val('');
				$('.form-handle').find('input').attr('readonly', true);
				$('.form-handle').find('select').attr('readonly', true);
			} else {
				$('#data_nasabah').val('').change();
				$('#DataNasabahContainer').hide();

				$('.form-handle').find('input').attr('readonly', false);
				$('.form-handle').find('select').attr('readonly', false);
			}
		});

		$(document).on('change', '#data_nasabah', function(e) {
            e.preventDefault();
			
			var id 	= $(this).val();
			var url = '{{ asset('images/') }}';
			
			if (id != '') {
				var nasabah = getObjects(data_nasabah, 'id', id)[0];

				$('input[name="ktp"]').val(nasabah.ktp);
				$('input[name="cif"]').val(nasabah.cif);
				$('input[name="rekening"]').val(nasabah.rekening);
				$('input[name="nama"]').val(nasabah.nama);
				$('textarea[name="alamat"]').val(nasabah.alamat);
				$('input[name="telepon"]').val(nasabah.telepon);

				$('#nasabah_ttd').parents('.form-group').find('img').first().attr('src', url+'/'+nasabah.ttd);
				$('#nasabah_foto').parents('.form-group').find('img').first().attr('src', url+'/'+nasabah.foto);

				$('#nasabah_ttd').attr('required', false);
				$('#nasabah_foto').attr('required', false);
			} else {
				$('.form-handle').find('input').val('');
				$('.form-handle').find('select').val('');
				$('.form-input').find('input').val('');
				$('.form-input').find('select').val('');
				$('.form-input').find('textarea').val('');

				$('#nasabah_ttd').parents('.form-group').find('img').first().attr('src', '{{ asset('images/img_default.png') }}');
				$('#nasabah_foto').parents('.form-group').find('img').first().attr('src', '{{ asset('images/img_default.png') }}');

				$('#nasabah_ttd').attr('required', true);
				$('#nasabah_foto').attr('required', true);
			}
		});

		$(document).on('keyup', '.kuasa1_form', function(e) {
			e.preventDefault();

			requiredCheck('.kuasa1_form');
			console.log('this keyup');
		});

		$(document).on('change', '.kuasa1_form', function(e) {
			e.preventDefault();

			requiredCheck('.kuasa1_form');
		});

		$(document).on('keyup', '.kuasa2_form', function(e) {
			e.preventDefault();

			requiredCheck('.kuasa2_form');
		});

		$(document).on('change', '.kuasa2_form', function(e) {
			e.preventDefault();

			requiredCheck('.kuasa2_form');
		});

		function requiredCheck(className) {
			required = false;
			$(className).each(function(index, el) {
				var val = $(el).val();
				if (!required && val != '') required = true;
            });

			$(className).prop('required', required);
		}

	</script>
@endsection


