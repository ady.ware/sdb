@extends('layouts.app')

@section('app.title')
    @yield('title')
@endsection

@section('app.style')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('css/prettify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker3-1.5.0.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sdb.css') }}" rel="stylesheet">
    @yield('style')
@endsection

@section('app.content')
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0; ">
            <a href="{{ url('/') }}" class="site_title">
                <div>
                    <img src="{{URL::to('/images/logo.png')}}" class="profile_img" style="width: 40px; height: 40px; vertical-align: middle; margin: 0 15%" >
                    <span class="pull-right" style="padding-right: 25px;">S D B</span>
                </div>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_info" style="width: 100%">
                <span></span>
                <h2>Selamat Datang, {{ Auth::user()->username }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3 class="invisible">Menu</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{url('beranda')}}">
                            <i class="fa fa-home"></i> Beranda
                        </a>
                    </li>
                    <li><a><i class="fa fa-minus-square"></i>Data<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('data') }}">Data</a></li>
                        </ul>
                    </li>
                    @if (Auth::user()->akses == 1)
                        <li><a><i class="fa fa-minus-square"></i>Laporan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('laporan-harian') }}">Laporan Harian</a></li>
                                <li><a href="{{ url('laporan-nasabah') }}">Laporan Nasabah</a></li>
                            </ul>
                        </li>
                    @endif
                    <li><a><i class="fa fa-minus-square"></i>User<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('ubah_password') }}">Ubah Password</a></li>
                            <li><a href="{{ url('logout') }}">Logout</a></li>
                        </ul>
                    </li>                    
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle" style="width: 10%;">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
        </nav>
    </div>
</div>

<!-- /top navigation -->
<div class="right_col" role="main">
    <div class="row">
        @yield('content')
    </div>
</div>

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/accounting.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/rangeSlider.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.scannerdetection.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker-1.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/option-charts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>

    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('js/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('js/prettify.js') }}"></script>
    <script type="text/javascript">
        var showSpinner = false;
    </script>

    @yield('script')
    <script type="text/javascript">
        $('.right_col').css('min-height', $('.left_col').css('height'));

        $(document).ready(function() {
            if (!showSpinner) {
                setTimeout(function() {
                    $('#spinner').hide();
                }, 500);
            }

            $(".select2_single").select2({
                width: '100%',
                allowClear: true
            });
        });

        function getObjects(obj, key, val) {
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getObjects(obj[i], key, val));    
                } else 
                //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
                if (i == key && obj[i] == val || i == key && val == '') { //
                    objects.push(obj);
                } else if (obj[i] == val && key == ''){
                    //only add if the object is not already in the array
                    if (objects.lastIndexOf(obj) == -1){
                        objects.push(obj);
                    }
                }
            }
            return objects;
        }

    </script>
@endsection
