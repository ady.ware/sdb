@extends('layouts.admin')

@section('title')
    <title>SDB | Pilih Data Laporan Kunjungan Harian SDB</title>
@endsection

@section('style')
    <style media="screen">
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Pilih Data Laporan Kunjungan Harian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <form action="{{ url('laporan-harian') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-1">
                            <div class="form-groupc">
                                <label class="control-label">Pilih Tanggal</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" name="tanggal_" value="" class="form-control tanggal-putih" id="single_cal3" readonly="" placeholder="{{ Util::date($date) }}">
                                <input class="form-control" type="hidden" name="tanggal" id="tanggal" value="{{ $date }}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <button class="btn btn-sm btn-success pull-right" id="btnPDF">
                                <i class="fa fa-file-pdf-o"></i> Save PDF
                            </button>
                            <button class="btn btn-sm btn-success pull-right" id="btnCetak">
                                <i class="fa fa-print"></i> Cetak
                            </button>
                            <button class="btn btn-sm btn-success pull-right" id="btnPilih" type="submit">
                                <i class="fa fa-eye"></i> Lihat Data
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Pilih Data Laporan Kunjungan Harian Tanggal {{ Util::date($date) }}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="dataTables_scroll" style="width=45vw; overflow-y=scroll">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0; width: 100%" id="tableData">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th colspan="2">Tanggal</th>
                                <th width="10%">No Box</th>
                                <th>Nama Nasabah</th>
                                <th>Petugas</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datas as $num => $data)
                            <tr id="{{$data->id}}">
                                <td>{{ $num+1 }}</td>
                                <td>{{ Util::date($data->created_at) }}</td>
                                <td>{{ Util::dateToTime($data->start_kunjungan, true) }}  -  {{ Util::dateToTime($data->end_kunjungan, true) }}</td>
                                <td>{{ $data->data->box }}</td>
                                <td>{{ $data->data->nasabah->nama }}</td>
                                <td>{{ $data->user->nama }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
			var table = $('#tableData').DataTable({
                            "bAutoWidth"        : false,
                            "bScrollCollapse"   : true,
                            "bProcessing"       : true,
                            dom                 : `<'row'<'col-sm-6'l><'col-sm-6'f>>
                                                    <'row'<'col-sm-12'tr>>
                                                    <'row'<'col-sm-5'i><'col-sm-7'p>>`,
                            "responsive"        : true,
                        });
        });

        $('#single_cal3').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            calender_style: "picker_2",
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
        }, 
        function(start, end, label) {
            var awal = (start.toISOString()).substring(0,10);
            $('#tanggal').val(awal);
        });

        $(document).on('click', '#btnCetak', function(e) {
            e.preventDefault();
            var tanggal = $('#tanggal').val();
            var win = window.open('{{ url('/cetak/laporan-harian/') }}'+`/${tanggal}`);
        });

        $(document).on('click', '#btnPDF', function(e) {
            e.preventDefault();
            var tanggal = $('#tanggal').val();
            window.location.href = '{{ url('/cetak/laporan-harian/pdf/') }}'+`/${tanggal}`;
        });
        
    </script>
@endsection
