@extends('layouts.admin')

@section('title')
    <title>SDB | Laporan Kunjungan Nasabah SDB</title>
@endsection

@section('style')
    <style media="screen">
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Pilih Data Laporan Kunjungan Nasabah</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <form id="formNasabah" action="{{ url('laporan-nasabah') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-2">
                            <div class="form-groupc">
                                <label class="control-label">Pilih Nasabah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="nasabah_id" id="nasabah_id" class="select2_single form-control" required="">
                                    <option value="">--Pilih Nasabah--</option>
                                    @foreach($nasabahs as $nasabah)
                                        <option value="{{$nasabah->id}}">{{$nasabah->nama}} [rekening:{{$nasabah->rekening}}] </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-groupc">
                                <label class="control-label">Pilih Box</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <select name="box" id="box" class="select2_single form-control" required="">
                                    <option value="">--Pilih Box--</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-sm btn-success pull-right" id="btnPDF">
                                <i class="fa fa-file-pdf-o"></i> Save PDF
                            </button>
                            <button class="btn btn-sm btn-success pull-right" id="btnCetak">
                                <i class="fa fa-print"></i> Cetak
                            </button>
                            <button class="btn btn-sm btn-success pull-right" id="btnPilih" type="submit">
                                <i class="fa fa-eye"></i> Lihat Data
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($dataShow)
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Laporan Kunjungan Nasabah</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Nomor Box</td>
                                    <td>{{ $arr['box'] }}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $arr['nama'] }}</td>
                                </tr>
                                <tr>
                                    <td>Rekening</td>
                                    <td>{{ $arr['rekening'] }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="dataTables_scroll" style="width=45vw; overflow-y=scroll">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0; width: 100%" id="tableData">
                                    <thead>
                                        <tr>
                                            <th width="10px">No</th>
                                            <th>Tanggal</th>
                                            <th>Waktu Kunjungan</th>
                                            <th>Pengunjung</th>
                                            <th>Petugas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $num => $data)
                                        <tr id="{{$data->id}}">
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ Util::date($data->updated_at) }}</td>
                                            <td>{{ Util::dateToTime($data->start_kunjungan, true) }}  -  {{ Util::dateToTime($data->end_kunjungan, true) }}</td>
                                            @if ($data->nasabah == 0)
                                                <td>{{ $data->data->nasabah->nama }}</td>
                                            @elseif ($data->kuasa1 == 1)
                                                <td>{{ $data->data->kuasa1->nama }}</td>
                                            @elseif ($data->kuasa2 == 2)
                                                <td>{{ $data->data->kuasa2->nama }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $data->user->nama }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Sukses!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'kosong')
    <script type="text/javascript">
        swal({
            title: 'Gagal!',
            text: 'Data yang anda pilih kosong!',
            timer: 3000,
            type: 'success'
        });
    </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
			var table = $('#tableData').DataTable({
                            "bAutoWidth"        : false,
                            "bScrollCollapse"   : true,
                            "bProcessing"       : true,
                            dom                 : `<'row'<'col-sm-6'l><'col-sm-6'f>>
                                                    <'row'<'col-sm-12'tr>>
                                                    <'row'<'col-sm-5'i><'col-sm-7'p>>`,
                            "responsive"        : true,
                        });

            var dataShow = '{{ $dataShow }}';
            if (dataShow) {
                var nasabah_id = '{{ $id }}';
                var box = '{{ $box }}';
                console.log(nasabah_id, box);
                $('#nasabah_id').val(nasabah_id).change();
                setTimeout(function() {
                    $('#box').val(box).change();
                }, 2000);
            }
        });

        $(document).on('change', '#nasabah_id', function(event) {
            event.preventDefault();

            var id  = $(this).val();
            var url = "{{ url('laporan-box') }}" + '/' + id;

            var $select = $('#box');
            $select.empty();
            $select.append($('<option value="">--Pilih Box--</option>'));

            $.get(url, function(data) {
                var boxs = data.boxs;

                for (var i = 0; i < boxs.length; i++) {
                    var box = boxs[i];
                    $select.append($('<option value="' + box + '">' + box + '</option>'));
                }
            });
        });

        $(document).on('click', '#btnCetak', function(e) {
            e.preventDefault();
            
            $('#formNasabah').attr('action', "{{ url('/cetak/laporan-nasabah/') }}").submit();
        });

        $(document).on('click', '#btnPDF', function(e) {
            e.preventDefault();

            $('#formNasabah').attr('action', "{{ url('/cetak/laporan-nasabah/pdf') }}").submit();
        });

    </script>
@endsection
