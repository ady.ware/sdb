@extends('layouts.admin')

@section('title')
    <title>SDB | Beranda</title>
@endsection

@section('style')
    <style media="screen">
        .thumbnail {
            padding: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="jumbotron col-md-12">
            <div class="col-md-12">
                <h1>EPOS</h1>
                <p>
                    Sistem Informasi Penjualan.
                </p>
            </div>
        </div>
    </div>
@endsection
