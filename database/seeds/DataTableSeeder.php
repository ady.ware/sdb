<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data')->delete();
        
        \DB::table('data')->insert(array (
            0 => 
            array (
                'id' => 1,
                'box' => 1,
                'jenis_sdb_id' => 2,
                'nasabah_id' => 1,
                'kuasa1_id' => null,
                'kuasa2_id' => null,
                'start_sewa' => '2019-12-26 21:01:35',
                'end_sewa' => '2020-12-26 21:01:36',
                'user_id' => 1,
                'created_at' => '2019-12-26 21:01:42',
                'updated_at' => '2019-12-26 21:01:43',
            ),
            1 => 
            array (
                'id' => 2,
                'box' => 2,
                'jenis_sdb_id' => 1,
                'nasabah_id' => 2,
                'kuasa1_id' => 1,
                'kuasa2_id' => 2,
                'start_sewa' => '2019-12-26 21:01:35',
                'end_sewa' => '2020-12-26 21:01:36',
                'user_id' => 1,
                'created_at' => '2019-12-26 21:01:42',
                'updated_at' => '2019-12-26 21:01:43',
            ),
        ));
        
        
    }
}