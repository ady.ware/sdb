<?php

use Illuminate\Database\Seeder;

class JenisSdbsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jenis_sdbs')->delete();
        
        \DB::table('jenis_sdbs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'SMALL',
                'keterangan' => '200.000',
                'created_at' => '2019-12-26 22:50:56',
                'updated_at' => '2019-12-26 22:50:58',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'MEDIUM',
                'keterangan' => '500.000',
                'created_at' => '2019-12-26 22:51:07',
                'updated_at' => '2019-12-26 22:51:08',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'LARGE',
                'keterangan' => '1.000.000',
                'created_at' => '2019-12-26 22:51:07',
                'updated_at' => '2019-12-26 22:51:08',
            ),
        ));
        
        
    }
}