<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'andrian',
                'nama' => 'andrian',
                'password' => '$2y$10$BA2ZQu19.vsgwDJWTdx3Mul.duBvnzkoaJCyJ.pMM2ZxHxKntA1xG',
                'akses' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-12-26 11:57:56',
                'updated_at' => '2019-12-26 15:06:11',
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'bloatware',
                'nama' => 'bloatware',
                'password' => '$2y$10$vr4gv.UO5QZp31VI45isGe9Besi./wYtzVRThE8UhbJS5tOLrXoW.',
                'akses' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-12-26 11:59:09',
                'updated_at' => '2019-12-26 15:06:11',
            ),
            2 => 
            array (
                'id' => 3,
                'username' => 'wisnu',
                'nama' => 'wisnu',
                'password' => '$2y$10$N2twXc1SfKRbstvqcj4.R.KT4ah5eTaNLv1j0avxwIK3vw06z3HPC',
                'akses' => 1,
                'remember_token' => NULL,
                'created_at' => '2019-12-26 14:42:11',
                'updated_at' => '2019-12-26 15:06:11',
            ),
        ));
    }
}