<?php

use Illuminate\Database\Seeder;

class KuasasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kuasas')->delete();
        
        \DB::table('kuasas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'Adam',
                'ktp' => '320504130319950002',
                'telepon' => '08566313',
                'catatan' => 'test',
                'ttd' => '1577455672.jpg',
                'foto' => '1577455924.jpg',
                'created_at' => '2019-12-26 20:59:55',
                'updated_at' => '2019-12-26 21:00:05',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'Rohmat',
                'ktp' => '330507130319950002',
                'telepon' => '086612381',
                'catatan' => 'test',
                'ttd' => '1577456052.jpg',
                'foto' => '11577455668.jpg',
                'created_at' => '2019-12-26 21:00:36',
                'updated_at' => '2019-12-26 21:00:39',
            ),
        ));
        
        
    }
}