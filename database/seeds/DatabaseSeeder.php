<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return vpopmail_del_domain(domain)
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(JenisSdbsTableSeeder::class);
        $this->call(NasabahsTableSeeder::class);
        $this->call(KuasasTableSeeder::class);
        $this->call(DataTableSeeder::class);
        $this->call(KunjungansTableSeeder::class);
    }
}
