<?php

use Illuminate\Database\Seeder;

class NasabahsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('nasabahs')->delete();
        
        \DB::table('nasabahs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'makruf',
                'ktp' => '330504130319950002',
                'cif' => '35123478',
                'rekening' => '8571230',
                'alamat' => 'Jakarta',
                'telepon' => '086798123881',
                'ttd' => '1577455667.jpg',
                'foto' => '1577455668.jpg',
                'created_at' => '2019-12-26 20:56:51',
                'updated_at' => '2019-12-26 20:56:52',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'syarif',
                'ktp' => '310504130319950002',
                'cif' => '23123478',
                'rekening' => '7171230',
                'alamat' => 'Jakarta',
                'telepon' => '086798123873',
                'ttd' => '1577455667.jpg',
                'foto' => '1577455668.jpg',
                'created_at' => '2019-12-26 20:56:51',
                'updated_at' => '2019-12-26 20:56:52',
            ),
        ));
        
        
    }
}