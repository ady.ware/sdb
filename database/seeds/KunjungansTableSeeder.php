<?php

use Illuminate\Database\Seeder;

class KunjungansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kunjungans')->delete();
        
        \DB::table('kunjungans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'data_id' => 1,
                'pengunjung' => 1,
                'start_kunjungan' => '2019-12-26 21:03:22',
                'end_kunjungan' => '2019-12-26 21:03:27',
                'user_id' => 2,
                'created_at' => '2019-12-26 21:03:30',
                'updated_at' => '2019-12-26 21:03:31',
            ),
            1 => 
            array (
                'id' => 2,
                'data_id' => 2,
                'pengunjung' => 1,
                'start_kunjungan' => '2019-12-26 21:03:22',
                'end_kunjungan' => '2019-12-26 21:03:27',
                'user_id' => 2,
                'created_at' => '2019-12-26 21:03:30',
                'updated_at' => '2019-12-26 21:03:31',
            ),
        ));
        
        
    }
}