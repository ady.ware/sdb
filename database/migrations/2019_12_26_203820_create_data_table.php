<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('box');
            $table->integer('jenis_sdb_id')->unsigned();
            $table->integer('nasabah_id')->unsigned();
            $table->integer('kuasa1_id')->nullable()->unsigned();
            $table->integer('kuasa2_id')->nullable()->unsigned();
            $table->dateTime('start_sewa');
            $table->dateTime('end_sewa');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('jenis_sdb_id')->references('id')->on('jenis_sdbs');
            $table->foreign('nasabah_id')->references('id')->on('nasabahs');
            $table->foreign('kuasa1_id')->references('id')->on('kuasas');
            $table->foreign('kuasa2_id')->references('id')->on('kuasas');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}