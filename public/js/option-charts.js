var option_tahun = `{
                        scales: {
                            yAxes: [{
                                ticks: {
                                    callback: function(value, index, values) {
                                        return 'Rp' + value.toLocaleString("id-ID");
                                    }
                                }
                            }]
                        },
                        tooltips: {
                            enabled: true,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var val = parseFloat(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                                    return 'Rp'+val.toLocaleString("id-ID");
                                }
                            }
                        }
                    }`;

// var option_bulan = 

// var option_tanggal = 

